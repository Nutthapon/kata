<?php
require_once'FizzBuzz.php';
	class FizzBuzzTest extends PHPUnit_Framework_TestCase
	{
		public $fizzBuzz;
		public function setup(){
			$this->fizzBuzz = new FizzBuzz();
		}
		public function testCalculateWith1ShouldReturn1(){
			$result = $this->fizzBuzz->calculate(1);
			$this->assertEquals(1, $result);
		}
		public function testCalculateWith2ShouldReturn2(){
			$result = $this->fizzBuzz->calculate(2);
			$this->assertEquals(2, $result);
		}
		public function testCalculateWith3ShouldReturnFizz(){
			$result = $this->fizzBuzz->calculate(3);
			$this->assertEquals('fizz', $result);
		}
		public function testCalculateWith4ShouldReturn4(){
			$result = $this->fizzBuzz->calculate(4);
			$this->assertEquals(4, $result);
		}
		public function testCalculateWith5ShouldReturnBuzz(){
			$result = $this->fizzBuzz->calculate(5);
			$this->assertEquals('buzz', $result);
		}
		public function testCalculateWith6ShouldReturnFizz(){
			$result = $this->fizzBuzz->calculate(6);
			$this->assertEquals('fizz', $result);
		}
		public function testCalculateWith10ShouldReturnBuzz(){
			$result = $this->fizzBuzz->calculate(10);
			$this->assertEquals('buzz', $result);
		}
		public function testCalculateWith15ShouldReturnFizzBuzz(){
			$result = $this->fizzBuzz->calculate(15);
			$this->assertEquals('fizzbuzz', $result);
		// }
	}
?>