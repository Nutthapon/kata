<?php
class Calculate{
	public function calculateEasy(){
		return 2;
	}
	public function catBracket($stringForCalculate){
		$stringAfterCutBracket = substr($stringForCalculate, 1, -1);
		return $stringAfterCutBracket;
	}
	public function explodePositionString($stringForCalculate){
		$arrayForeExplodeString = explode(' ', $stringForCalculate);
		return $arrayForeExplodeString;
	}
	public function calculateValuesInArray($stringForCalculate){
		if ($stringForCalculate[0] == '+') {
			$result = $stringForCalculate[1]+$stringForCalculate[2];
			return $result;
		}elseif ($stringForCalculate[0] == '-') {
			$result = $stringForCalculate[1]-$stringForCalculate[2];
			return $result;
		}elseif ($stringForCalculate[0] == '*') {
			$result = $stringForCalculate[1]*$stringForCalculate[2];
			return $result;
		}elseif ($stringForCalculate[0] == '/') {
			$result = $stringForCalculate[1]/$stringForCalculate[2];
			return $result;
		}elseif ($stringForCalculate[0] == '%') {
			$result = $stringForCalculate[1]%$stringForCalculate[2];
			return $result;
		}elseif ($stringForCalculate[0] == '.') {
			return "helloworld";
		}
	}
	public function calculateString($stringForCalculate){
		$resultCatBracket = $this->catBracket($stringForCalculate);
		$resultExplodePositionStringToArray = $this->explodePositionString($resultCatBracket);
		$resultCalculate = $this->calculateValuesInArray($resultExplodePositionStringToArray);
		return $resultCalculate;
	}
}
?>