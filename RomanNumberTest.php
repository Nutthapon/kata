﻿<?php
require_once('RomanNumber.php');
class RomanNumberCalculator extends PHPUnit_Framework_TestCase
{
    public $myRomanNumber;
    public function setup(){
        $this->myRomanNumber = new RomanNumberCalculate();
    }
    public function testRomanIandI()
    {
        $result = $this->myRomanNumber->testRomanCalculateEasy('I I');
        $this->assertEquals('II', $result);
    }
    // public function testRomanIandV()
    // {
    //     $result = $this->myRomanNumber->testSplitRomanNumber('I V');
    //     $this->assertEquals(['I','V'], $result);
    // }
    public function testSplitAndSumRomanIandII()
    {
        $result = $this->myRomanNumber->testSplitRomanNumber('I II');
        $this->assertEquals('III', $result);
    }
    // public function testconvertRomanNumberI()
    // {
    //     $result = $this->myRomanNumber->convertRomanToNumber('V');
    //     $this->assertEquals([5],$result);
    // }
    // public function testconvertRomanNumberL()
    // {
    //     $result = $this->myRomanNumber->convertRomanToNumber('L');
    //     $this->assertEquals(0,$result);
    // }
    public function testconvertRomanNumberII()
    {
        $result = $this->myRomanNumber->convertRomanToNumber('IVI');
        $this->assertEquals([1,5,1], $result);
    }
    public function testSumNumber(){
        $result = $this->myRomanNumber->sumNumber([1,5,1]);
        $this->assertEquals(7, $result);    
    }
}
?>