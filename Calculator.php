<?php
require_once("Calculate.php");
class CalculatorTest extends PHPUnit_Framework_TestCase
{
    public $stringForCalculate;
    public function setup(){
    	$this->stringForCalculate = new Calculate();
    }
    public function testCalculatorString()
    {
        $result = $this->stringForCalculate->calculateEasy("(+ 1 1)");
        $this->assertEquals(2, $result);
    }
    public function testCatBracket()
    {
        $result = $this->stringForCalculate->catBracket("(+ 1 1)");
        $this->assertEquals("+ 1 1", $result);
    }
    public function testExplodePositionStringToArray()
    {
        $result = $this->stringForCalculate->explodePositionString("+ 1 1");
        $this->assertEquals(['+',1,1], $result);
    }
    public function testCalculateValuesInArray()
    {
        $result = $this->stringForCalculate->calculateValuesInArray(['+',1,1]);
        $this->assertEquals(2, $result);
    }
    public function testCalculator1Plus1()
    {
    	$result = $this->stringForCalculate->calculateString("(+ 1 1)");
        $this->assertEquals(2, $result);
    }
    public function testCalculator12Plus34()
    {
    	$result = $this->stringForCalculate->calculateString("(+ 12 34)");
        $this->assertEquals(46, $result);
    }
    public function testCalculator1Minus2()
    {
    	$result = $this->stringForCalculate->calculateString("(- 1 2)");
        $this->assertEquals(-1, $result);
    }
    public function testCalculator5Multiply6()
    {
    	$result = $this->stringForCalculate->calculateString("(* 5 6)");
        $this->assertEquals(30, $result);
    }
    public function testCalculator30Divide5()
    {
    	$result = $this->stringForCalculate->calculateString("(/ 30 5)");
        $this->assertEquals(6, $result);
    }
    public function testCalculator30Divide4()
    {
    	$result = $this->stringForCalculate->calculateString("(/ 30 4)");
        $this->assertEquals(7.5, $result);
    }
    public function testCalculator30modulo4()
    {
    	$result = $this->stringForCalculate->calculateString("(% 30 4)");
        $this->assertEquals(2, $result);
    }
    public function testCalculatorHelloFuseWorld()
    {
    	$result = $this->stringForCalculate->calculateString("(. “hello” “world”)");
        $this->assertEquals('helloworld', $result);
    }
}
?>